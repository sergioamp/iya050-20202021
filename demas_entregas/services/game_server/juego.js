/*
funcionalidad del juego:

//////////////////////////////////////////////////////////////////////////////////////////////

el juego es un incremental; tiene "picos", los cuales hacen daño a "materiales" segun su
nivel y version, y "materiales" los cuales tienen vida y otorgan "oro" al llegar su vida
a 0.

el siguiente material se consigue comprandolo con oro.

en cualquier momento se puede "reiniciar", volviendo tanto el pico al nivel mas bajo
como el material, y perdiendo todo el oro, pero si al hacerlo tienes el oro total obtenido
suficiente, conseguiras el siguiente pico.

hay que hacer clic para comprar las cosas, pero el daño se hace solo a lo largo del tiempo.

el cliente es quien se encargaria de llamar a "nextTick" una vez cada segundo.

//////////////////////////////////////////////////////////////////////////////////////////////

nuevaPartida(){

    construye un estadoPartida a 0

    return(
        estadoPartida
    )
}

prestigio(){

        el jugador quiere realizar el prestigio.
        deberia poder ya que eso lo controla el front
        si puede, retorna el estado de la partida actualizada

        return(
            estadoPartida(actualizada)
        )
}

nextTick(estadoPartida){

    hace avanzar el juego en un frame

    debe:
        hacer daño al material y si lo rompe, obtener el oro

    return (
        estadoPartida(actualizado)
    )
}

comprarSiguienteMaterial(estadoPartida, nombreMaterial){

    intenta comprar el siguiente material disponible.

    si puede, lo hace que el material actual sea el que era el siguiente y
    actualiza el siguiente material y 

     return (
        estadoPartida(actualizado)
    )

    (se supone que el front es quien maneja si el jugador cumple o no 
    con las condiciones necesarias para comprar)
}

subirNivel(estadoPartida){

    intenta subir el nivel del pico.
    deberia poder, ya que eso lo comprueba previamente el front

    si puede, actualiza la partida

    return (
        estadoPartida(actualizado)
    )
}
*/

let juego = {
    nuevaPartida: () => {
        return (
            `{
                "oroActual": "0",
                "oroAcumulado": "0",
                "pico": {
                    "nivel": "0"
                  },
                "material": {
                    "nivel": "0",
                    "vidaActual": "10"
                  },
                "prestigio": "0"
              }`
        );
    },
    prestigio: (partida) => {
        if ((parseInt(partida.prestigio) + 1) * 1000 > parseInt(partida.oroAcumulado)) {
            // no deberia poderse dar el caso
            return partida;
        } else {
            let prestigioAnterior = parseInt(partida.prestigio);

            let partida2 = JSON.parse(juego.nuevaPartida());
            partida2.prestigio = "" + (prestigioAnterior + 1);

            return partida2;
        }
    },
    nextTick: (partida) => {
        partida.material.vidaActual = "" + (parseInt(partida.material.vidaActual) - (parseInt(partida.pico.nivel) + 1));

        if (parseInt(partida.material.vidaActual) <= 0) {
            let oroGanado = (parseInt(partida.material.nivel) + 1) * (parseInt(partida.prestigio) + 1);

            partida.oroActual = "" + (parseInt(partida.oroActual) + oroGanado);
            partida.oroAcumulado = "" + (parseInt(partida.oroAcumulado) + oroGanado);
            partida.material.vidaActual = "" + ((parseInt(partida.material.nivel) + 1) * 10);
        }
        return partida;
    },
    comprarSiguienteMaterial: (partida) => {
        if (parseInt(partida.material.nivel + 1) * 100 > parseInt(partida.oroActual)) {
            // no deberia poderse dar el caso
            return partida;
        } else {
            partida.oroActual = "" + (parseInt(partida.oroActual) - (parseInt((partida.material.nivel + 1) * 100)));
            partida.material.nivel = "" + (parseInt(partida.material.nivel) + 1);
            partida.material.vidaActual = "" + ((parseInt(partida.material.nivel) + 1) * 10);// reinicio la vida del material

            return partida;
        }
    },
    subirNivel: (partida) => {
        if ((parseInt(partida.pico.nivel) + 1) * 10 > parseInt(partida.oroActual)) {
            // no deberia poderse dar el caso
            return partida;
        } else {
            partida.oroActual = "" + (parseInt(partida.oroActual) - ((parseInt(partida.pico.nivel) + 1) * 10));
            partida.pico.nivel = "" + (parseInt(partida.pico.nivel) + 1);

            return partida;
        }
    }
}
module.exports = juego;
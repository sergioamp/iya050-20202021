const juego = require('./juego');
const express = require('express');
const PORT = process.env.PORT || 3050;

const app = express();

app.use(express.urlencoded());
app.use(express.json());



//Route
app.get('/', (req, res) => {
    res.send("Welcome to my API<br>" +
        "<br>" +
        "Metodos disponibles:<br>" +
        "nuevaPartida<br>" +
        "cargarPartida?partida={partida}<br>" +
        "prestigio?partida={partida}<br>" +
        "nextTick?partida={partida}<br>" +
        "comprarsiguienteMaterial?partida={partida}<br>" +
        "subirNivel?partida={partida}<br>" +
        "guardarPartida?partida={partida}" +
        "<br><br>by Sergio Domínguez");
});

//All customers
app.get('/nuevaPartida', (req, res) => {

    res.send(juego.nuevaPartida());
});

app.get('/cargarPartida', (req, res) => {
    res.send(juego.cargarPartida(req.query.partida));
});
app.get('/prestigio', (req, res) => {
    res.send(juego.prestigio(JSON.parse(req.query.partida)));
});
app.get('/nextTick', (req, res) => {
    res.send(JSON.stringify(juego.nextTick(JSON.parse(req.query.partida))));
});
app.get('/comprarSiguienteMaterial', (req, res) => {
    res.send(juego.comprarSiguienteMaterial(JSON.parse(req.query.partida)));
});
app.get('/subirNivel', (req, res) => {
    res.send(juego.subirNivel(JSON.parse(req.query.partida)));
});
app.get('/guardarPartida', (req, res) => {
    res.send(JSON.stringify(juego.guardarPartida(req.query.partida)));
});

app.listen(PORT, () => { console.log(`Server running on port: ${PORT}`) });
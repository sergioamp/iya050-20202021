const juego = require('./juego.js');
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

////////////////////////////////////////////////////////

chai.use(chaiHttp);
const url = 'http://localhost:3050';

let parsear = (json) => JSON.stringify(JSON.parse(json));

// tests del juego:
describe('crear una partida nueva: ', () => {
    it('deberia subir de prestigio', (done) => {
        let partida = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
        expect(parsear(juego.nuevaPartida())).to.be.equal(parsear(partida));
        done();
    });
});


// tests del servidor:
describe('conectar con el servidor: ', () => {
    it('deberia devolver un status 200', (done) => {
        chai.request(url)
            .get('/nuevaPartida')
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });
});

describe('prestigio: ', () => {
    let partida1 = `{"oroActual":"0","oroAcumulado":"99999","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
    let partida1Prestigo = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"1"}`
    let partida2 = `{"oroActual": "0","oroAcumulado": "0","pico": {"nivel": "0"},"material": {"nivel": "0","vidaActual": "10"},"prestigio": "0"}`
    it('deberia subir de prestigio', (done) => {
        chai.request(url)
            .get(`/prestigio?partida=${partida1}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida1Prestigo));
                done();
            });

    });
    it('deberia subir de prestigio solo si puede', (done) => {
        chai.request(url)
            .get(`/prestigio?partida=${partida2}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida2));
                done();
            });
    });
});


describe('next tick: ', () => {
    let partida1 = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
    let partida1Response = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"9"},"prestigio":"0"}`
    let partida2 = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"1"},"prestigio":"0"}`
    let partida2Response = `{"oroActual":"1","oroAcumulado":"1","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`

    it('deberia hacerle danho al material', (done) => {
        chai.request(url)
            .get(`/nextTick?partida=${partida1}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida1Response));
                done();
            });
    });
    it('deberia destruir el material; dando oro y este recuperando toda la vida', (done) => {
        chai.request(url)
            .get(`/nextTick?partida=${partida2}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida2Response));
                done();
            });
    });
});

describe('comprar siguiente material: ', () => {
    let partida1 = `{"oroActual":"110","oroAcumulado":"110","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
    let partida1Response = `{"oroActual":"10","oroAcumulado":"110","pico":{"nivel":"0"},"material":{"nivel":"1","vidaActual":"20"},"prestigio":"0"}`
    let partida2 = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"1"},"prestigio":"0"}`

    it('sube lvl del material cuando puede', (done) => {
        chai.request(url)
            .get(`/comprarSiguienteMaterial?partida=${partida1}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida1Response));
                done();
            });
    });
    it('no sube lvl del material cuando no puede', (done) => {
        chai.request(url)
            .get(`/comprarSiguienteMaterial?partida=${partida2}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida2));
                done();
            });
    });
});

describe('subir nivel pico: ', () => {
    let partida1 = `{"oroActual":"110","oroAcumulado":"110","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
    let partida1Response = `{"oroActual":"100","oroAcumulado":"110","pico":{"nivel":"1"},"material":{"nivel":"0","vidaActual":"10"},"prestigio":"0"}`
    let partida2 = `{"oroActual":"0","oroAcumulado":"0","pico":{"nivel":"0"},"material":{"nivel":"0","vidaActual":"1"},"prestigio":"0"}`

    it('sube lvl del pico cuando puede', (done) => {
        chai.request(url)
            .get(`/subirNivel?partida=${partida1}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida1Response));
                done();
            });
    });
    it('no sube lvl del pico cuando no puede', (done) => {
        chai.request(url)
            .get(`/subirNivel?partida=${partida2}`)
            .end(function (err, res) {
                expect(parsear(res.text)).to.be.equal(parsear(partida2));
                done();
            });
    });
});







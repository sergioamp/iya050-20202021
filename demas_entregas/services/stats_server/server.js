<<<<<<< HEAD
const { GraphQLServer } = require("graphql-yoga");

const applicationId = "applicationId:SergioDominguez";
const urlBase =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";

let putPair = (key, value) => {
  return putPair2(key, value, 5);
};

let putPair2 = (key, value, contador) => {
  if (contador === 0) return false;
  fetch(
    new Request(urlBase + "pairs/" + key, {
      method: "PUT",
      body: value,
      headers: new Headers().append("x-application-id", applicationId)
    })
  )
    .then(response => response)
    .then(function(data) {
      if (data.status < 200 || data.status >= 300) {
        return putPair2(key, value, contador--);
      }
      return true;
    });
};

let getPair = key => {
  return getPair2(key, 5);
};

let getPair2 = (key, contador) => {
  if (contador === 0) {
    return null;
  }
  fetch(
    new Request(urlBase + "pairs/" + key, {
      method: "GET",
      headers: new Headers().append("x-application-id", applicationId)
    })
  )
    .then(function(response) {
      return response;
    })
    .then(function(data) {
      if (data.status < 200 || data.status >= 300) {
        return getPair2(key, contador--);
      } else {
        data.json().then(function(json) {
          return json;
        });
      }
    });
};

let getAllPairs = prefix => {
  return getAllPairs2(prefix, 5);
};

let getAllPairs2 = (prefix, contador) => {
  if (contador === 0) {
    return null;
  }
  fetch(
    new Request(urlBase + "pairs/" + prefix, {
      method: "GET",
      headers: new Headers().append("x-application-id", applicationId)
    })
  )
    .then(function(response) {
      return response;
    })
    .then(function(data) {
      if (data.status < 200 || data.status >= 300) {
        return getAllPairs2(prefix, contador--);
      } else {
        data.json().then(function(json) {
          return json;
        });
      }
    });
};

let picosNombre = [
  "Pico desgastado",
  "Pico básico",
  "Pico profesional",
  "Escavadora industrial",
  "Tuneladora planetaria",
  "Destructor de mundos"
];

let picosRes = [1, 3, 5, 10, 20, 35];

let picosOro = [
  "(x) => x + 1",
  "(x) => 3 * x + 1",
  "(x) => 20 * x + 1",
  "(x) => x * x + 1",
  "(x) => 2 * x * x + 1",
  "(x) => x * x * x + 1"
];

let materialesNombre = ["Tierra", "Piedra", "Cobre", "Hierro", "Plata", "Oro"];

let materialesHp = [5, 100, 1000, 80000, 200000, 1000000];

let materialesOro = [1, 50, 120, 800, 1300, 999999];

let materialesOroDesbloqueo = [0, 20, 100, 1000, 2000, 10000];

var schema = `
    type Material {
        nombre: String!
        maxHp: Int!
        oro: Int!
    }
    type Pico {
        nombre: String!
        nivel: Int!
        oroNivel: String!
        danho: Int!
    }
    type Resurreccion {
        multiplicadorDanho: Int!
    }
    type Stats {
        resurrecciones: [Resurreccion!]!
        materialMaximo: Material!
    }
    type Estado {
        oroActual : Int!
        picoActual: Pico!
        resurreccionActual: Resurreccion!
    }
    type Partida { 
        id: Int!
        stats: Stats!
        estado: Estado!
        enUso: Boolean!
    }
    type Query {
        partida(id: ID!): Partida
    }
    type Mutation {
        guardarPartida: Partida!
        ponerPartidaEnUso(partida: Partida!): Partida! 
        quitarPartidaEnUso(partida: Partida!): Partida!        
    }
`;

const resolvers = {
  Material: {
    nombre: id => {
      return materialesNombre[id];
    },
    maxHp: nombre => {
      return materialesHp[nombre];
    },
    oro: nombre => {
      return materialesOro[nombre];
    },
    oroDesbloqueo: nombre => {
      return materialesOroDesbloqueo[nombre];
    }
  },
  Pico: {
    nombre: num => {
      return picosNombre[num];
    },
    resurreccion: num => {
      // a que nº de resurreccion se desbloquea
      return picosRes[num];
    },
    oroNivel: num => {
      // cuanto oro/lvl hace falta para subirlo de lvl
      return picosOro[num];
    }
  },
  Resurreccion: {
    multiplicadorDanho: numero => {
      return numero * numero;
    }
  },
  Stats: {
    resurrecciones: partidaId => {
      // pido al server las resurrecciones
      let json = getPair(partidaId);
      if (json !== null) {
        return json.resurreciones; 
      } else {
        return -1;
      }
    },
    materialMaximo: partidaId => {
      // pido al server el material maximo historico al que ha llegado
      let json = getPair(partidaId);
      if (json !== null) {
        return json.materialMax;
      } else {
        return -1;
      }
    }
  },
  Estado: {
    picoActual: estado => estado.picoActual,
    oroActual: estado => estado.oroActual,
    resurreccionActual: estado => estado.resurreccionActual
  },
  Partida: {
    id: partida => partida.id,
    stats: partida => partida.stats,
    estado: partida => partida.estado,
    enUso: partida => partida.enUso
  },
  Query: {
    partida: id => getPair(`partida-${id})`)
  },
  Mutation: {
    guardarPartida: partida => {
      putPair(partida.id, partida);
    },
    ponerPartidaEnUso: partida => {
      partida.enUso = true;
      putPair(partida.id, partida);
      return partida;
    },
    quitarPartidaEnUso: partida => {
      partida.enUso = false;
      putPair(partida.id, partida);
      return partida;
    }
  }
};

const server = new GraphQLServer({ schema, resolvers });

server.start({
  playground: "/",
  port: process.env.PORT
});
=======
// TBI
>>>>>>> f6713bbba210dc2fc7d27f519e7f43936a989653

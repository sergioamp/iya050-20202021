import React from "react";

const BarraProgreso = () => {
    return (
        <div id="myProgress">
            <div id="myBar"></div>
        </div>

    );
};
export default BarraProgreso;
import React from 'react';
import { withSnackbar } from 'notistack';
import "./css/juego.css"



function Cuadrado(props) {
    return (
        <button className="cuadrado" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Tablero extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            cuadrados: Array(9).fill(null),
            xIsNext: true,
        };




    }


    handleClick(i) {
        const cuadrados = this.state.cuadrados.slice();
        if (calcularGanador(cuadrados) || cuadrados[i]) {
            return;
        }
        cuadrados[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            cuadrados: cuadrados,
            xIsNext: !this.state.xIsNext,
        });
    }

    renderCuadrado(i) {
        return (
            <Cuadrado
                value={this.state.cuadrados[i]}
                onClick={() => this.handleClick(i)}
            />
        );
    }


    render() {
        const ganador = calcularGanador(this.state.cuadrados);
        let status;
        if (ganador) {
            status = "";
            this.props.crearAviso(`Ha ganado ${ganador}`);
        } else {
            status = 'Siguiente Jugador: ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div>
                <div className="status">{status}</div>
                <div className="tablero-fila">
                    {this.renderCuadrado(0)}
                    {this.renderCuadrado(1)}
                    {this.renderCuadrado(2)}
                </div>
                <div className="tablero-fila">
                    {this.renderCuadrado(3)}
                    {this.renderCuadrado(4)}
                    {this.renderCuadrado(5)}
                </div>
                <div className="tablero-fila">
                    {this.renderCuadrado(6)}
                    {this.renderCuadrado(7)}
                    {this.renderCuadrado(8)}
                </div>
            </div>
        );
    }
}

class Juego extends React.Component {
    constructor(props) {
        super(props);

        this.crearAviso = (msg) => {
            this.props.enqueueSnackbar(msg, {
                variant: 'defautl',
                persist: true,
            });
        }
    }

    render() {
        return (
            <div className="juego">
                <Tablero
                    crearAviso={this.crearAviso.bind(this)}
                />
            </div>
        );
    }
}


function calcularGanador(cuadrados) {
    const lineas = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lineas.length; i++) {
        const [a, b, c] = lineas[i];
        if (cuadrados[a] && cuadrados[a] === cuadrados[b] && cuadrados[a] === cuadrados[c]) {
            return cuadrados[a];
        }
    }
    return null;
}

export default withSnackbar(Juego);
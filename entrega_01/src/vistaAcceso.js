import React from 'react';
import { withSnackbar } from 'notistack';
import "./css/vistaAcceso.css"

class VistaAcceso extends React.Component {
    constructor(props) {
        super(props);



        /*
        * 
        */
        this.comprobarDatos = () => {
            let usuario = document.getElementById("usuarioAcceso");
            let pw = document.getElementById("pwAcceso");

            if (usuario === null
                || usuario === undefined
                || usuario.value === null
                || usuario.value === undefined
                || usuario.value === '') {
                this.props.enqueueSnackbar(`Introduzca su usuario, por favor.`, {
                    variant: 'default',
                    persist: false,
                });
                return false;
            }
            if (pw === null
                || pw === undefined
                || pw.value === null
                || pw.value === undefined
                || pw.value === '') {
                this.props.enqueueSnackbar(`Introduzca su contraseña, por favor.`, {
                    variant: 'default',
                    persist: false,
                });
                return false;
            }
            return true;
        };

        // funcion a ejecutar cuando le demos a registrarnos
        this.registroClicHandler = () => {
            this.props.cambiarVista(1);
        };

        // funcion a ejecutar cuando le demos a login
        this.loginClicHandler = () => {
            if (!this.comprobarDatos()) {
                return;
            }
            this.props.enqueueSnackbar(`Logueando, por favor espere...`, {
                variant: 'default',
                persist: false,
            })

            let usuario = document.getElementById('usuarioAcceso').value;
            let pw = document.getElementById('pwAcceso').value;

            // el usuario ha introducido todos los datos
            // ahora toca preguntarle al servidor si son correctos
            let contexto = this;
            this.props.login(usuario, pw)
                .then(function (data) {
                    let estado = data.status;
                    if (estado >= 200 && estado < 300) {
                        estado = 200;
                    }
                    if (estado >= 400 && estado < 500) {
                        estado = 400;
                    }
                    switch (estado) {
                        case (200):
                            // ok
                            data.text().then(function (text) {
                                let token = text.split(':').pop().slice(1, -2)
                                window.localStorage.setItem('token', token)
                                console.log('token: ' + token);

                                // ahora recupero los datos del usuario
                                contexto.props.obtainData(token)
                                    .then(function (data) {
                                        estado = data.status;
                                        if (estado >= 200 && estado < 300) {
                                            estado = 200;
                                        }
                                        if (estado >= 400 && estado < 500) {
                                            estado = 400;
                                        }
                                        switch (estado) {
                                            case (200):
                                                data.json().then(function (json) {
                                                    contexto.props.loginSinServer(json.firstName, json.username);
                                                    contexto.props.cambiarVista(2);
                                                    contexto.props.enqueueSnackbar(`Bienvenido/a ${json.firstName}`, {
                                                        variant: 'default',
                                                        persist: false,
                                                    })
                                                })
                                                break;
                                            case (400):
                                            // logeo pero no puedo recuperar datos por algun motivo que no voy a manejar
                                            case (500):
                                                // logeo pero no puedo recuperar datos porque el server revento
                                                contexto.props.enqueueSnackbar('Error logueando, inténtelo de nuevo en unos momentos.', {
                                                    variant: 'warning',
                                                    persist: false,
                                                })
                                                contexto.props.cambiarVista(0);
                                                this.status.recordarPW = false;
                                                break;
                                        }
                                    })
                            });
                            window.localStorage.setItem('recordarPW', 'false')
                            console.log("entrando");
                            //contexto.props.cambiarVista(2);
                            break;
                        case (400):
                            // usuario no existe
                            contexto.props.enqueueSnackbar('Usuario o contraseña incorrecto.', {
                                variant: 'warning',
                                persist: false,
                            });
                            break;
                        case (500):
                            // gatillazo del servidor
                            contexto.props.enqueueSnackbar('Error en el servicio de autenticación,\ninténtelo de nuevo en breves momentos.', {
                                variant: 'warning',
                                persist: false,
                            });
                            break;
                    }
                })
        };
    }
    render() {
        return (
            <div>
                <div>
                    <div className="tituloAcceso">
                        3 EN RAYA
                    </div>
                    <div className="lineaTextoAcceso">
                        <span className="textoAcceso">Usuario:</span><input className="cajaInputAcceso" id="usuarioAcceso" type="text"></input>
                    </div>
                    <div className="lineaTextoAcceso">
                        <span className="textoAcceso">Contraseña:</span><input className="cajaInputAcceso" id="pwAcceso" type="password"></input>
                    </div>
                    <div className="lineaBotonesAcceso">
                        <button className="botonLoginAcceso" onClick={() => { this.loginClicHandler() }}>Login</button>
                        <button className="botonRegistroAcceso" onClick={this.registroClicHandler}>Registrarse</button>
                    </div>
                </div>
            </div>
        )
    }
}


export default withSnackbar(VistaAcceso)
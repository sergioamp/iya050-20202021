import React from 'react';
import VistaPrincipal from './VistaPrincipal';
import VistaAcceso from './VistaAcceso';
import VistaRegistro from './VistaRegistro';
import { init } from 'emailjs-com';
import "./css/controladorGeneral.css"

const direccion = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/";

/*
*
*/
function mandarHTTP(mime, parametros, cabeceras, cuerpo) {
    let req = null;
    if (cabeceras !== null
        && cuerpo === null) {
        req = new Request(
            direccion + parametros,
            {
                method: mime,
                headers: cabeceras,
            }
        );
    }
    if (cabeceras === null
        && cuerpo !== null) {
        req = new Request(
            direccion + parametros,
            {
                method: mime,
                body: cuerpo
            }
        );
    }
    if (cabeceras !== null
        && cuerpo !== null) {
        req = new Request(
            direccion + parametros,
            {
                method: mime,
                headers: cabeceras,
                body: cuerpo
            }
        );
    }

    switch (mime) {
        case ('GET'):
        case ('POST'):
        case ('DELETE'):
            return fetch(req)
                .then(function (response) {
                    return response;
                })
                .then(function (data) {
                    if ((data.status < 200 || data.status >= 300)
                        && data.status !== 409
                        && data.status !== 500) {
                        console.log(`ERROR POCO COMÚN, status: ${data.status} statusText: ${data.statusText}`);
                        return - 1;
                    }
                    return data;
                })
                .catch(function (err) {
                    console.log("err: " + err);
                    return 'error';
                });
        default:
            return -1;
    }
    //
}

class ControladorGeneral extends React.Component {
    constructor(props) {
        super(props);
        init("user_LCMA9wrgFIFhpkl0ehJAW");




        this.state = {
            vista: 0,
            recordarPW: false
        };



        /*
        * codigo estados:
        * --
        * 0 => acceso (defecto)
        * --
        * 1 => registro
        * --
        * 2 => principal 
        * --
        */
        this.cambiarVista = (estado) => {
            return this.setState({
                vista: estado,
            })
        };

        this.cambiarRecordarPW = (recordar) => {
            return this.setState({ recordarPW: recordar })
        };
        this.getRecordarPW = () => {
            return this.state.recordarPW;
        };


        /*
        *
        */
        this.login = (usuario, pw) => {
            console.log('login');
            let body = `{
                    "password": "${pw}"
                  }`;
            return mandarHTTP('POST', `users/${usuario}/sessions`, null, body);
        };
        /*
       *
       */
        this.registro = (nombre, usuario, pw) => {
            console.log('registro');
            let body = `{
                    "firstName": "${nombre}",
                    "password": "${pw}"
                  }`;
            return mandarHTTP('POST', `users/${usuario}`, null, body);
        };
        /*
       *
       */
        this.logout = (token) => {
            console.log('logout');
            var myHeaders = new Headers();
            myHeaders.append('Authorization', token);

            return mandarHTTP('DELETE', `sessions/${token}`, myHeaders, null);
        };
        /*
      *
      */
        this.obtainData = (token) => {
            console.log('obtainData from' + '\nTOKEN: ' + token);
            var myHeaders = new Headers();
            myHeaders.append('Authorization', token);

            return mandarHTTP('GET', 'user', myHeaders, null);
        };

        this.loginSinServer = (nombre, usuario) => {
            window.localStorage.setItem('nombre', nombre)
            window.localStorage.setItem('usuario', usuario)
        };

        /////////////////////////////////////////////////////////////////////////////////////

        /*
        * Elimino cualquier posible dato de una sesion anterior, por si acaso
        */
        if (window.localStorage.getItem('nombre') !== null
            && window.localStorage.getItem('nombre') !== undefined) {
            window.localStorage.setItem('nombre', '')
        }
        if (window.localStorage.getItem('usuario') !== null
            && window.localStorage.getItem('usuario') !== undefined) {
            window.localStorage.setItem('usuario', '')
        }
        if (window.localStorage.getItem('apellido') !== null
            && window.localStorage.getItem('apellido') !== undefined) {
            window.localStorage.setItem('apellido', '')
        }

        /*
        *   AUTOLOGIN
        */
        let contexto = this;
        if (window.localStorage.getItem('recordarPW') !== null
            && window.localStorage.getItem('recordarPW') !== undefined) {
            // recordarPW existe
            if (window.localStorage.getItem('recordarPW') === 'true') {
                this.state.recordarPW = true;
                console.log("usuario recordado. entrando...");
                // hay que entrar automanticamente
                // 
                // miro si hay token guardado
                if (window.localStorage.getItem('token') !== null
                    && window.localStorage.getItem('token') !== undefined
                    && window.localStorage.getItem('token') !== '') {
                    contexto.obtainData(window.localStorage.getItem('token'))
                        .then((data) => {
                            let estado = data.status;
                            if (estado >= 200 && estado < 300) {
                                estado = 200;
                            }
                            if (estado >= 400 && estado < 500) {
                                estado = 400
                            }
                            switch (data.status) {
                                case (200):
                                    // salio todo flaman
                                    // hay que entrar y hay datos
                                    data.json().then(function (json) {
                                        console.log('obtainedData: ' + json);
                                        contexto.loginSinServer(json.firstName, json.username);
                                        contexto.cambiarVista(2);
                                    })
                                    break;
                                case (400):
                                // el token esta pocho
                                case (500):
                                    // el server va peor que los de riot
                                    contexto.cambiarVista(0);
                                    break;
                            }
                        })
                }
            }
        }
    }





    render() {
        let pagina = null;
        switch (this.state.vista) {
            case (0):
                // acceso
                pagina = <VistaAcceso
                    cambiarVista={this.cambiarVista.bind(this)}
                    login={this.login.bind(this)}
                    obtainData={this.obtainData.bind(this)}
                    cambiarRecordarPW={this.cambiarRecordarPW.bind(this)}
                    loginSinServer={this.loginSinServer.bind(this)}
                />;
                break;
            case (1):
                // registro
                pagina = <VistaRegistro
                    cambiarVista={this.cambiarVista.bind(this)}
                    registro={this.registro.bind(this)}
                    login={this.login.bind(this)}
                />;
                break;
            case (2):
                // principal
                pagina = <VistaPrincipal
                    cambiarVista={this.cambiarVista.bind(this)}
                    logout={this.logout.bind(this)}
                    cambiarRecordarPW={this.cambiarRecordarPW.bind(this)}
                    getRecordarPW={this.getRecordarPW.bind(this)}
                />;
                break;
        }
        return (
                <div className="MarcoGeneral">
                    {pagina}
                </div>
        )
    }
}
export default ControladorGeneral

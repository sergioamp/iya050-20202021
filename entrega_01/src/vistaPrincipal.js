import React from 'react';
import { withSnackbar } from 'notistack';
import Juego from './juego';
import "./css/vistaPrincipal.css"





class VistaPrincipal extends React.Component {
  constructor(props) {
    super(props);

    /*
    * 
    */
    this.eliminarRecordarSesion = () => {
      window.localStorage.setItem('token', '');
      window.localStorage.setItem('nombre', '');
      window.localStorage.setItem('recordarPW', 'false');
    };

    /*
    *
    */
    this.desloguearseClicHandler = () => {
      this.props.enqueueSnackbar('Te has desconectado.');
      this.eliminarRecordarSesion();
      this.props.cambiarVista(0);
      console.log("ASDASD");
    };

    /*
if (window.localStorage.getItem('recordarPW') !== null
      && window.localStorage.getItem('recordarPW') !== undefined) {
      if (window.localStorage.getItem('recordarPW') === 'false') {
        document.getElementById('recordarPw').checked = true;
      }
    }
    */

  }


  render() {
    return (
      <div className="MarcoGeneralVistaPrincipal">
        <div className="Titulo">Vista principal de {window.localStorage.getItem('nombre')}</div>
        <div>
          <button className="botonLogout" onClick={() => this.desloguearseClicHandler()}>Desloguearse</button>
          <button className="botonRecordarSesion" onClick={() => {
            window.localStorage.setItem('recordarPW', 'true');
            this.props.enqueueSnackbar('La próxima vez entrará automáticamente.', {
              variant: 'default',
              persist: false,
            })
          }}>Recordar Sesión</button>
        </div>
        <div>

        </div>
        <div className="Juego">
          <Juego />
        </div>
      </div >
    )
  }
}
export default withSnackbar(VistaPrincipal)
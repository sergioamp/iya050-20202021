import React from 'react';
import { init } from 'emailjs-com';
import emailjs from 'emailjs-com';
import { withSnackbar } from 'notistack';
import Button from '@material-ui/core/Button';
import "./css/vistaRegistro.css"

class VistaRegistro extends React.Component {
  constructor(props) {
    super(props);

    init("user_LCMA9wrgFIFhpkl0ehJAW");

    const action = () => (

      <Button onClick={() => {
        document.getElementById('nombre').value = 'Susana';
        document.getElementById('apellido').value = 'Oria';
        document.getElementById('usuario').value = 'user123asdqqqa1s2d3';
        document.getElementById('pw').value = '123';
        document.getElementById('pw2').value = '123';
        document.getElementById('correo').value = 'ejemplo@noexiste.com';
      }}>
        Autorrellenar
      </Button>
    );


    /*
    * se manda un correo de 'verificacion' a la direccion dada
    * retorna true o false segun ha salido todo bien o no
    */
    this.mandarCorreo = (props, direccion, nombre, apellido, usuario, pw) => {
      let noMandar = false;
      if (noMandar) {
        console.log('correo "mandado"');
        return;
      }
      var templateParams = {
        to_name: nombre,
        to_secondName: apellido,
        to_userName: usuario,
        to_pw: pw,
        to_email: direccion
      };
      emailjs.send('service_t2wwehw', 'template_vkhhp5c', templateParams)
        .then(function (response) {
          console.log(`SUCCESS!`, response.status, response.text);
          props.cambiarVista(2);
          return true;
        }, function (error) {
          console.log('FAILED...', error);
          props.enqueueSnackbar('Error al mandar el correo de comprobación (¿Dirección equivocada?).', {
            variant: 'error',
            persist: false,
          });
          return false;
        });
    };


    this.comprobarDatos = () => {
      if (document.getElementById('nombre').value === undefined
        || document.getElementById('nombre').value === null
        || document.getElementById('nombre').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado el nombre.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (document.getElementById('apellido').value === undefined
        || document.getElementById('apellido').value === null
        || document.getElementById('apellido').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado el apellido.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (document.getElementById('usuario').value === undefined
        || document.getElementById('usuario').value === null
        || document.getElementById('usuario').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado el usuario.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      switch (document.getElementById('usuario').value.charAt(0)) {
        case ('0'):
        case ('1'):
        case ('2'):
        case ('3'):
        case ('4'):
        case ('5'):
        case ('6'):
        case ('7'):
        case ('8'):
        case ('9'):
          this.props.enqueueSnackbar('El nombre de usuario no puede empezar por número.', {
            variant: 'warning',
            persist: false,
            action
          });
          return
      }
      if (document.getElementById('pw').value === undefined
        || document.getElementById('pw').value === null
        || document.getElementById('pw').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado la contraseña.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (document.getElementById('pw2').value === undefined
        || document.getElementById('pw2').value === null
        || document.getElementById('pw2').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado comprobar la contraseña', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (document.getElementById('pw').value != document.getElementById('pw2').value) {
        this.props.enqueueSnackbar('Las contraseñas no coinciden', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (document.getElementById('correo').value === undefined
        || document.getElementById('correo').value === null
        || document.getElementById('correo').value === '') {
        this.props.enqueueSnackbar('Se le ha olvidado la dirección de correo.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      if (!document.getElementById('correo').value.includes('@')) {
        this.props.enqueueSnackbar('Dirección de correo no válida.', {
          variant: 'warning',
          persist: false,
          action
        });
        return false;
      }
      return true;
    };

    this.funcionBotonRegistro = (props) => {
      let esto = this;
      if (this.comprobarDatos()) {
        // se mira que los datos esten introducidos y correctos
        let nombre = document.getElementById('nombre').value;
        let usuario = document.getElementById('usuario').value;
        let pw = document.getElementById('pw').value;

        // se manda la solicitud de registro al servidor
        this.props.registro(nombre, usuario, pw)
          .then(function (data) {
            switch (data.status) {
              case (201):
                // va way
                // se le manda el correo al usuario
                esto.mandarCorreo(
                  props,
                  document.getElementById('correo').value,
                  nombre,
                  document.getElementById('apellido').value,
                  usuario,
                  pw);
                props.login(usuario, pw)
                  .then(function (data) {
                    switch (data.status) {
                      case (201):
                        // todo bien
                        window.localStorage.setItem('usuario', usuario);
                        window.localStorage.setItem('pw', pw);
                        window.localStorage.setItem('nombre', document.getElementById('nombre').value);
                        window.localStorage.setItem('apellido', document.getElementById('apellido').value);
                        data.text().then(function (text) {
                          window.localStorage.setItem('token', text.split(':').pop().slice(1, -2))
                        });
                        window.localStorage.setItem('recordarPw', 'false')
                        props.cambiarVista(2);
                        break;
                      case (409):
                        // error (usuario incorrecto y similares)
                        // no deberia suceder nunca ya que significa que 
                        // el servidor ha rechazado los mismos datos que 
                        // ha aceptado antes
                        props.enqueueSnackbar('Error inexperado.', {
                          variant: 'error',
                          persist: false,
                        });
                        break;
                      case (500):
                        // otro petardazo
                        let reintentarLogin = () => {
                          props.login(usuario, pw)
                            .then(function (data) {
                              switch (data.status) {
                                case (201):
                                  // por fin entra
                                  window.localStorage.setItem('usuario', usuario);
                                  window.localStorage.setItem('pw', pw);
                                  window.localStorage.setItem('nombre', document.getElementById('nombre').value);
                                  window.localStorage.setItem('apellido', document.getElementById('apellido').value);
                                  window.localStorage.setItem('recordarPw', 'false')
                                  props.cambiarVista(2);
                                  return;
                                case (409):
                                  // error (usuario incorrecto y similares)
                                  // no deberia suceder nunca ya que significa que 
                                  // el servidor ha rechazado los mismos datos que 
                                  // ha aceptado antes
                                  props.enqueueSnackbar('Error inexperado.', {
                                    variant: 'error',
                                    persist: false,
                                  });
                                  return;
                                case (500):
                                  // otro petardazo
                                  // no hago nada y dejo q lo vuelva a intentar
                                  break;
                              }
                            });
                        };
                        for (let i = 0; i < 5; i++) {
                          reintentarLogin();
                        }
                        props.cambiarVista(0);
                        props.enqueueSnackbar('Error al contactar con el servidor.\nSe ha registrado, pero ha sucedido un error al hacer login', {
                          variant: 'error',
                          persist: false,
                        });
                        break;
                    }
                  });
                break;
              case (409):
                // usuario ya existe
                props.enqueueSnackbar('Usuario no disponible.', {
                  variant: 'warning',
                  persist: false,
                });
                break;
              case (500):
                // petardazo
                props.enqueueSnackbar('Error conectando con el servidor. Intentelo de nuevo más tarde.', {
                  variant: 'error',
                  persist: false,
                });
                break;
            }
          });
      }
    };
  }
  render() {
    return (
      <div>
        <div className="textoTitulo">REGISTRO
        </div>
        <div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Nombre:</span> <input className="cajaInputRegistro" id="nombre" type="text"></input>
          </div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Apellido:</span> <input className="cajaInputRegistro" id="apellido" type="text"></input>
          </div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Usuario:</span> <input className="cajaInputRegistro" id="usuario" type="text"></input>
          </div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Contraseña:</span> <input className="cajaInputRegistro" id="pw" type="password"></input>
          </div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Repetir Contraseña:</span> <input className="cajaInputRegistro" id="pw2" type="password"></input>
          </div>
          <div className="lineaInputRegistro">
            <span className="textoRegistro">Correo Electrónico:</span> <input className="cajaInputRegistro" id="correo" type="text"></input>
          </div>
          <div className="lineaBotonesRegistro">
            <button id="botonRegistro" onClick={() => { this.funcionBotonRegistro(this.props) }
            }>Registro</button>
            <button onClick={() => this.props.cambiarVista(0)}>Cancelar</button>
          </div>
        </div>
      </div >
    )
  }
}


export default withSnackbar(VistaRegistro)



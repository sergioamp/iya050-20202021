import React from 'react';
import VistaPrincipal from './VistaPrincipal';
import VistaAcceso from './VistaAcceso';
import VistaRegistro from './VistaRegistro';
import {
    login, registro, logout, obtainData
} from "./ControladorGeneral";
//
import 'regenerator-runtime/runtime'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import { useHistory } from "react-router-dom";







export default function Inicio() {

    function comprobarAutologin() {
        if (window.localStorage.getItem('recordarPW') !== null
            && window.localStorage.getItem('recordarPW') !== undefined) {
            // recordarPw existe
            if (window.localStorage.getItem('recordarPW') === 'true') {
                console.log("usuario recordado. entrando...");
                // recordarPw existe y esta a true
                window.localStorage.setItem('token', '')
                if (window.localStorage.getItem('token') !== null
                    && window.localStorage.getItem('token') !== undefined) {
                    obtainData(window.localStorage.getItem('token'))
                        .then(function (data) {
                            switch (data.value) {
                                case (201):
                                    // se han recuperado satisfactoriamente los datos
                                    // hay que entrar
                                    data.text()
                                        .then(function (text) {
                                            console.log("TEXT INICIO: " + text);
                                            this.props.login(
                                                //
                                                // tengo en 'text' la informacion del usuario tras pedir sus datos al server
                                                // tengo que extraerla para hacer login y actualizar el token
                                                //
                                                // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 
                                                // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 
                                                //
                                                //
                                            )
                                                .then(function (data) {
                                                    switch (data.status) {
                                                        case (201):
                                                            // noice
                                                            data.text().then(function (text) {
                                                                window.localStorage.setItem('token', text.split(':').pop().slice(1, -2))
                                                                // he pedido el login, se ha aceptado y tengo en localstorage el token
                                                                return 2;
                                                            });
                                                            break;
                                                        case (409):
                                                        // no deberia pasar esto nunca, salvo bug malo
                                                        case (500):
                                                            // c rompio
                                                            return 0;
                                                    }
                                                })
                                        })
                                    break;
                                case (409):
                                // el token no es valido
                                case (500):
                                    // el server se porta fatal
                                    return 0;
                            }
                        })
                } else {
                    // se supone que hay que entrar automaticamente,
                    // pero no hay token de usuario
                    return 0;
                }
            } else {
                // hay recordarPW y esta a false
                return 0;
            }
        } else {
            // no existe el recordar usuario
            return null;
        }
    }

    return (
        <Router>
            <div>
                Práctica 1 Programación web II
                <div>
                    <button onClick={() => {
                        window.localStorage.setItem('recordarPW', 'true')
                        console.log('R PW: ' + window.localStorage.getItem('recordarPW'))
                    }}>RecordarPW ON</button>

                    <button onClick={() => {
                        window.localStorage.setItem('recordarPW', 'false')
                        console.log('R PW: ' + window.localStorage.getItem('recordarPW'))
                    }}>RecordarPW OFF</button>
                </div>
                <div>
                    <Switch>
                        <Route exact path="/"><VistaAcceso /></Route>
                        <Route exact path="/principal"><VistaPrincipal /></Route>
                        <Route exact path="/registro"> <VistaRegistro /></Route>
                    </Switch>
                </div>
            </div>
        </Router>
    );
}
import React from 'react';
import ReactDom from 'react-dom';
import ControladorGeneral from './ControladorGeneral';
import { SnackbarProvider } from 'notistack';


ReactDom.render(
    <SnackbarProvider>
        <ControladorGeneral />
    </SnackbarProvider>, document.getElementById("root"))